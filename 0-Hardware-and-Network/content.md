---
marp: true
header: 'ReProgram - Training'
---
<!-- paginate: false -->

# 0. Hardware and Network
Ab, 2023

---
<!-- paginate: true -->

## Introduction

- Name, goals
- There is not magic
- Let's write less code
- Safe space (harassment-free)
  - for everyone, regardless of gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion (or lack thereof), or technology choices

---

## GNU/Linux

- “Jeez, everything in Unix is a file!”
- A file can be a network connection, a FIFO, a pipe, a terminal, a real on-the-disk file, or just about anything else. Everything in Unix is a file!


<!-- footer: https://beej.us/guide/bgnet/html -->

---

<!-- footer: '' -->

## Linux Kernel

![height:13cm](./application.png)

---

## Linux Distribution

![height:13cm](./distribution.jpg)

---

![](./gldt1210.png)

---

## Terminal, Commands

- Hands on

---


## Memory, Disk, Processor

- Tower of Hanoi
- Hands on

---

## Network Protocols

- DNS: Domain Name System (DNS Protocol)
  - Translates domain names to the numerical IP address
- IP: Internet Protocol
  - Delivers packets from source to destination based on the IP address
- TCP: Transmission Control Protocol
  - Connection-oriented
  - All data arrives in order and error-free
- UDP: User Datagram Protocol
  - Connectionless
  - Order (or even delivery) isn’t guaranteed

---

## Network Protocols

- Unix Socket - a form of communication between two processes that appears as a file on disk
  - Stream Socket (TCP)
  - Datagram Socket (UDP)
  - Raw sockets - which don’t have any restrictions, and are used for implementing different protocols and utilities that need to inspect low-level network traffic

- Hands on

---

## Deepening & Improving

- https://www.khanacademy.org/computing/code-org/computers-and-the-internet/how-computers-work/v/khan-academy-and-codeorg-introducing-how-computers-work
- https://wizardzines.com/comics (Julia Evans)
- https://phoenixnap.com/kb/wp-content/uploads/2022/03/linux-commands-cheat-sheet-pnap.pdf

