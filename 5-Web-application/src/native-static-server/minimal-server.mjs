import * as fs from "fs";
import * as http from "http";

http
  .createServer(async (req, res) => {
    const file = req.url === '/' ? 'index.html' : req.url;
    const content = fs.readFileSync(`${process.cwd()}/static/${file}`);

    res.end(content)
  })
  .listen(8000);

console.log(`Server running at http://127.0.0.1:8000/`);
