---
marp: true
header: 'ReProgram - Training'
---
<!-- paginate: false -->

# 5. Web application
Ab, 2023

---

<!-- paginate: true -->

## Previously on "ReProgram"

- Interpreter, compiler, transpiler
- Container (Docker) vs Virtual Machines (VM)
- Constants, Variables
- string, number, boolean
- array, object (attributes, methods)
- if, else, elseif
- HTML, CSS, Javascript

---

## HTML, CSS, Javascript

![height:13cm](./html-css.jpeg)

---

## HTML, CSS, Javascript

![height:13cm](./html-css-js-michael.jpeg)

---

## HTML, CSS, Javascript, Bootstrap

![height:13cm](./html-css-js-superman.jpeg)

---

## Webserver

- Javascript static files server
- External packages
- npm, npx
- relative and absolute paths
- hands on

---

## Deploy

- Server (EC2)
- SSH 
- SCP
- RSYNC
- SFTP
- hands on

---

## GIT

- git clone
- git pull
- git add
- git commit
- git push

---

## Deepening & Improving

- https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web
- https://developer.mozilla.org/en-US/docs/Learn/Server-side/Node_server_without_framework
- https://github.com/http-party/http-server
- https://git-scm.com/doc
- https://www.namecheap.com