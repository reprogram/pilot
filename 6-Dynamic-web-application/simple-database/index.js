const express = require('express')
const fs = require('fs')

const app = express()
const filename = 'database.txt'

app.use(express.urlencoded({ extended: true }));

app.get('/', function(req, res) {
  const content = fs.readFileSync(filename)

  res.send(content);
});

app.post('/create', function(req, res){
  const name = req.body.name
  const content = fs.readFileSync(filename)
  
  const result = content + name + '\n'
  
  fs.writeFileSync(filename, result)

  res.redirect('/')
});

app.post('/update', function(req, res){
  const content = fs.readFileSync(filename).toString()
  
  const result = content.replace(req.body.name, req.body.newName)

  fs.writeFileSync(filename, result)

  res.redirect('/')
});

app.delete('/delete/:name', function(req, res){
  const name = req.params.name
  
  const content = fs.readFileSync(filename).toString()

  const result = content.split('\n')
        .filter((v) => v !== name)
        .join('\n');

  fs.writeFileSync(filename, result)

  res.redirect('/')
});

app.listen(3000);

console.log('Express started on port http://localhost:3000');
