---
marp: true
header: 'ReProgram - Training'
---
<!-- paginate: false -->

# 6. Dynamic Web application
Ab, 2023

---

<!-- paginate: true -->

## Previously on "ReProgram"

- Interpreter, compiler, transpiler
- Container (Docker), Virtual Machines (VM)
- Constants, Variables
- string, number, boolean , array, object
- HTML, CSS, Javascript
- Domain, DNS, Web Server, Git

---

## Team Roles

- Project Manager
- Product Owner / Software Analyst / Business Analyst
- Engineer Manager
- User Experience (UX)
- Designer
- Software Engineer (Mobile / Frontend / Backend / Full Stack)
- Quality Assurance Engineer (QA)
- Data Engineer
- Data Science

---

## Database

- CRUD (Create, Read, Update, Delete)
- Relational
  - MySQL
  - PostgreSQL
- Non-relational Database (NoSQL Database)
  - Redis
  - Cassandra
  - MongoDB

---

## Relational Database

- ACID
- Atomicity: All transactions must succeed or fail completely and cannot be left partially complete, even in the case of system failure
- Consistency: The database must follow rules that validate and prevent corruption at every step
- Isolation: Concurrent transactions cannot affect each other
- Durability: Transactions are final, and even system failure cannot “roll back” a complete transaction

---

## Non-Relational Database

- CAP theorem (Some database also follow ACID)
- Consistency: Every request receives either the most recent result or an error
- Availability: Every request has a non-error result
- Partition tolerance: Any delays or losses between nodes do not interrupt the system operation

---

## Scalability

- Vertical Scale
- Horizontal Scale

---

## Database

- Hands on (Most simple database in the world)

---

## Database

- SQLite
- MySQL
- Hands on

---

## Deepening & Improving

- https://medium.com/@softwaresisterz/a-quick-guide-to-members-of-a-tech-team-4f19708849fa
- https://careers.google.com/teams/engineering-technology
- https://www.prisma.io/dataguide/intro/comparing-database-types
- https://www.sqlite.org
- https://www.mysql.com
- https://dev.mysql.com/doc/index-other.html
- https://expressjs.com/en/starter/installing.html
