# Challenge: Create a database in a text file

## What do you need?

1. Five files: 
  create.js (Insert data in the database file)
  read.js (Read the data from the database file) 
  update.js (Update data in the database file)
  delete.js (Remove data in the database file)
  database.txt (The database itself)

2. A database schema:
  We are going to have a database to store Name and Score, separated by line and , 

  Ex:
  Name,Score
  Jose,5
  Joao,10
  Carlos,3

3. Pass the parameter as arguments to the files:
  Ex:

  `node create.js Joao 10`
  (This will insert one line in the database file in the format "Joao,10")

  `node update.js Joao 1`
  (This will update the line in the database file to "Joao,1")

  `node read.js`
  (This will read all lines in the database)

  `node delete.js Joao`
  (This will remove the line in the database)


## Questions that you can ask to your search engine :P

1. How to read a file in node
2. How to write in a file using node
3. Get command line arguments in node
4. How to concatenate values in node
