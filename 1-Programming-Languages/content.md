---
marp: true
header: 'ReProgram - Training'
---
<!-- paginate: false -->

# 1. Programming Languages
Ab, 2023

---
<!-- paginate: true -->

## Previously on "ReProgram"

- Memory, Processor and disk
- Linux, Linux Distributions
- TCP, UDP

---

## Bash

- pwd, whoami, $HOME, /etc/group
- $PATH, .bashrc, .bash_history. aliases

---

## Bash

- Hands on

---

## Permissions

- user, group, all
- Letters (r, w, x)
- numbers (4, 6, 7)
- chmod (ugoa / rwxXst / 000)
- chown

---

## Permissions

![height:13cm](./chmod.png)

---

## Permissions

![height:13cm](./permission-letter-number.png)

---

## Permissions

- Hands on

---


## Docker

- Container vs. VM
- Memory, CPU, Storage (Isolation)

---

## Docker

- docker images
- docker run / stop / rm
- docker ps

---

## Programming Languages

- High-Level, Low-Level
- Compiler (AOT - Ahead-of-time_compilation)
  - Translates code from a high-level programming language into machine code before the program runs
- Interpreter
  - Translates code from a high-level programming language into machine code line-by-line as the code runs
- Virtual Machine, Transpiler, JIT

---

## Programming Languages

- Memory, memory, memory
- Garbage collection
- Memory leak

---

## Programming Languages

- Hands on (https://codeberg.org/reprogram/pilot)
- strace

---


## How to choose a Programming Languages?

---

## Deepening & Improving

- https://www.redhat.com/sysadmin/linux-file-permissions-explained
- https://en.wikipedia.org/wiki/Ahead-of-time_compilation
- https://en.wikipedia.org/wiki/Just-in-time_compilation
- https://en.wikipedia.org/wiki/Java_virtual_machine
- https://google.github.io/comprehensive-rust/memory-management.html
