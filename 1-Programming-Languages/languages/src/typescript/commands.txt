drunv node:alpine sh

npm install -g typescript

tsc hello.ts

node hello.js

---------------------------------------

drunv denoland/deno bash

deno run hello.ts
