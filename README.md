# ReProgram

## Overview
The program provides training that will allow participants to choose from different career tracks in the technology sector, facilitate diversity, and create equality in the workplace.

It aims to empower members to achieve their personal and professional goals by bringing their authentic selves wherever they go. It strives to diversify the tech ecosystem by breaking down barriers, creating spaces, and connecting communities to support and empower underrepresented groups. We believe we are stronger when we value the differences among us.

The project encourages a sense of belonging that comes from both giving and contributing to your community. Focused on education, on reciprocity, and that everyone has something to teach and learn. We embrace diversity and inclusivity in the classroom, taking the opportunity to learn from one another, creating supportive spaces in order to strengthen the tech community, free from all forms of discrimination.

It works to improve social integration and employability, where empowerment and representation are key.

The program aims to create a long-term, measurable impact in society. We know that by helping people make tangible, positive changes in their lives, we can improve their social mobility and reduce inequality.

## Goals

1. Training of underrepresented groups to work in the technology sector
2. Introducing the participants to real-world opportunities of learning on the job

## Audience
Individuals who identify themselves as Queer, BIPOC or find themselves in the status of being a refugee, asylum seekers, stateless, and migrants in need of international protection and/or at risk of social exclusion