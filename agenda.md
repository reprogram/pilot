---
marp: true
header: 'ReProgram - Agenda'
---


# Agenda
Ab, 2023

---

**11-07** - Hardware and Network
**18-07** - Programming Languages
**25-07** - Programming 101
**01-08** - Web application
**08-08** - Web application
**15-08** - Web application
**22-08** - Dynamic web application
**29-08** - Dynamic web application
**05-09** - Cloud Computing
**12-09** - Cloud Computing (Async tasks)
**19-09** - Cloud Computing (Automation)
**26-09** - Scrum & Jira (Extra)

---

# 0. Hardware and Network (11-07)
  - Introduction
  - GNU/Linux - Free software
  - Memory, Disk, Processor
  - Linux, Terminal, Commands
  - Network Protocols
  - RFCs

---

# 1. Programming Languages (18-07)
  - Bash, Permissions
  - Script, Interpretation, Transpilation, Compilation, Virtual Machines

---

# 2. Programming 101 (25-07)
  - Why are we going to use Javascript?
  - Constants, Variables
  - Functions, Arguments
  - Control structures (Condition, Loop)

---

# 3. Web application (01-08)
  - HTML, CSS (https://www.csszengarden.com)
  - Web Developer Tools

---

# 4. Web application (08-08)
  - Javascript
  - IDE

---

# 5. Web application (15-08)
  - Frameworks (Bootstrap, jQuery)
  - Deploy (EC2, SSH, RSYNC, GIT, SFTP)
  - Git

---

# 6. Dynamic web application (22-08)
  - Backend/Frontend
  - Databases, SQL
  - CRUD

---

# 7. Dynamic web application (29-08)
  - From 0 to Production
    - Analyze
    - Prototype
    - Architecture
    - Develop
    - Deploy

---

# 8. Cloud Computing (05-09)
  - Account, Permissions
  - Environment
  - VM, Managed Services

---

# 9. Cloud Computing (Async tasks) (12-09)
  - Queue
  - Pub/Sub
  - Events

---

# 10. Cloud Computing (Automation) (19-09)
  - Serverless
  - Serverless Framework
  - Deploy
  - QA

---

# 11. Scrum & Jira (26-09)