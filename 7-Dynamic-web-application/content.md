---
marp: true
header: 'ReProgram - Training'
---
<!-- paginate: false -->

# 7. Dynamic Web application
Ab, 2023

---

<!-- paginate: true -->

## Previously on "ReProgram"

- HTML, CSS, Javascript
- Domain, DNS, Web Server, Git
- Databases

---

## MVP -  Minimum viable product

- Understand your customer interest without fully developing the product
- Less effort and expense on a product that will not succeed in the market

---

## Quiz Project

- Ab - Client
- Everybody - Product Owner, Business Analyst 
- Team 1 - UX / Designer / Frontend
- Team 2 - Backend
- Team 3 - Infrastructure / Database

---

## Stack

- Quant UX
- Kickstand UI
- Express.js
- MySQL

---

## Deepening & Improving

- https://quant-ux.com
- https://kickstand-ui.com/getting-started/installation.html
- https://dev.mysql.com/doc/index-other.html
- https://expressjs.com/en/starter/installing.html
