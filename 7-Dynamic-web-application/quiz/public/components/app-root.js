import './rp-header.js'

class AppRoot extends HTMLElement {
  render(html) {
    this.querySelector('#main-content').innerHTML = html
  }

  connectedCallback() {
    const html = `
      <rp-header></rp-header>
      <ks-row>
        <ks-column class="col-3"></ks-column>
        <ks-column class="col-6" id="main-content"></ks-column>
        <ks-column class="col-3"></ks-column>
      </ks-row>
    `
    this.innerHTML = html

    new Navigo('/')
      .on('/', () => this.render('home'))
      .on('/about', () => this.render('about'))
      .resolve()
  }
}

customElements.define('app-root', AppRoot)
