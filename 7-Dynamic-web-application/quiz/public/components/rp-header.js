class RpHeader extends HTMLElement {
  connectedCallback() {
    const html = `
      <ks-menu-bar tagline="Super Quiz" inverted  collapse="xl">
        <ks-menu-row>
          <ks-menu-item>
            <a href="/" data-navigo>Home</a>
          </ks-menu-item>
          <ks-menu-item>
            <a href="/about" data-navigo>About</a>
          </ks-menu-item>
        <ks-menu-row>
      </ks-menu-bar>
    `
    this.innerHTML = html
  }
}

customElements.define("rp-header", RpHeader)