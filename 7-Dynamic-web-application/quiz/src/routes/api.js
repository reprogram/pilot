import express from 'express'
const router = express.Router()

router.get('/', function (req, res, next) {
  res.json({root: true, message: 'API ROOT'})
})

export default router
