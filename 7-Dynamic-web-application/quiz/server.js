import express from 'express'
import apiRouter from './src/routes/api.js'

const app = express()

app.use(express.json());

app.use('/api', apiRouter)

app.use(express.static('public'))

app.get('/*', (req, res) => {
  res.sendFile(process.cwd() + '/public/index.html')
})

app.listen(3000)

console.log('listening on port 3000')
