---
marp: true
header: 'ReProgram - Training'
---
<!-- paginate: false -->

# 2. Programming 101
Ab, 2023

---

<!-- paginate: true -->

## Previously on "ReProgram"

- Bash, permission
- Interpreter, compiler, vm, transpiler

---

## Why are we going to use Javascript?

- High Level, Interpreted and it has JIT
- Big community
- Run in all browsers (V8, SpiderMonkey, JavaScriptCore)
- it can be used in the Frontend, Backend, Game Development, Mobile 
Development, Virtual reality (VR), Artificial intelligence (AI)
- Stable API (Application Programming Interface)

---

## Constants, Variables

- const, let
- string, number, boolean
- array, object (attributes, methods)

---

## Constants, Variables

- Hands on
- process, process.env, process.argv

---


## Control structures (Condition, Loop)

- if, else, elseif
- switch, case, break
- for, foreach, while, do

---

## Functions, Arguments

- function, arguments, optional values
- sync, async, Promise
- recursive functions

---

## External packages

- npm
- package.json, package-lock.json

---

## Deepening & Improving

- https://developer.mozilla.org/en-US/docs/Web/JavaScript/JavaScript_technologies_overview
- https://learnprogramming.online
- https://learnjavascript.online
- https://nodejs.org/dist/latest-v18.x/docs/api/