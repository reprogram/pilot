// node hello.js
// node hello.js John
// NAME=John node hello.js

let name = process.argv[2]; // Or let name = process.env.NAME;

if (!name) {
   name = 'World';
}

console.log('Hello, %s', name);
