// node calc.js sum 3 5
const operation = process.argv[2];
const n1 = parseFloat(process.argv[3]);
const n2 = parseFloat(process.argv[4]);
let result;

if (operation == 'sum') {
    result = n1 + n2
}

if (operation == 'sub') {
    result = n1 - n2
}

if (operation == 'mul') {
    result = n1 * n2
}

if (operation == 'div') {
    result = n1 / n2
}

console.log(result)
